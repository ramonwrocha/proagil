using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProAgil.Repository;

namespace ProAgil.WebAPI.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class PalestranteController: ControllerBase {
        private readonly IProAgilRepository _repo;
        public PalestranteController(IProAgilRepository repo) {
            _repo = repo;
        }

        [HttpGet("{PalestranteId}")]
        public async Task<IActionResult> Get(int PalestranteId)
        {
            try
            {
                var results = await _repo.GetPalestranteAsync(PalestranteId, true);
                return Ok(results);
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database fail");
            }
        }

        [HttpGet("{Name}")]
        public async Task<IActionResult> Get(string Name)
        {
            try
            {
                var results = await _repo.GetAllPalestrantesAsyncByName(Name, true);
                return Ok(results);
            }
            catch (System.Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database fail");
            }
        }
    }
}
