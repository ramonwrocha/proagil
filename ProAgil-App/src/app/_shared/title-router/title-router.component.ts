import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title-router',
  templateUrl: './title-router.component.html',
  styleUrls: ['./title-router.component.css']
})
export class TitleRouterComponent implements OnInit {

  @Input() title;

  constructor() { }

  ngOnInit(): void {
  }

}
