import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventosService } from 'src/app/_services/eventos.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Evento } from 'src/app/_models/evento.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { defineLocale, BsLocaleService, ptBrLocale } from 'ngx-bootstrap';
defineLocale('pt-br', ptBrLocale);
import { ToastrService } from 'ngx-toastr';
import { Constants } from 'src/app/_helpers/utils/constants';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})

export class EventosComponent implements OnInit, OnDestroy {
  titulo = 'Eventos';
  fileName = '';
  filtro: string;
  eventosFiltrados: Evento[];
  eventos: Evento[];
  evento: Evento;
  showImg = true;
  unsubscribe = new Subject();
  registerForm: FormGroup;
  file: File;

  constructor(
    private eventosService: EventosService,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private toastr: ToastrService) {
    this.localeService.use('pt-br');
  }

  get filtroLista() {
    return this.filtro;
  }

  set filtroLista(value: string) {
    this.filtro = value.toLocaleLowerCase();
    this.eventosFiltrados = this.filtroLista ? this.filtrarEvento() : this.eventos;
  }

  ngOnInit(): void {
    this.validation();
    this.getAllEventos();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private getAllEventos(): void {
    this.eventosService.getAllEventos().pipe(takeUntil(this.unsubscribe))
      .subscribe((result: Evento[]) => {
        result.forEach(item => item.imagemURL = `${Constants.BASE_URL}/resources/images/${item.imagemURL}?rnd=${Math.random().toString()}`);
        this.eventosFiltrados = result;
        this.eventos = result;
      }, err => console.log(err));
  }

  public openModal(template: any) {
    this.evento = null;
    this.fileName = '';
    this.registerForm.reset();
    template.show(template);
  }

  public openModalEdit(template: any, evento: Evento): void {
    this.evento = Object.assign({}, evento);
    this.evento.imagemURL = '';
    this.registerForm.patchValue(this.evento);
    template.show(template);
  }

  public closeModal(template): void {
    this.evento = null;
    this.registerForm.reset();
    template.hide();
  }

  public showHideImg(): void {
    this.showImg = !this.showImg;
  }

  private filtrarEvento(): Evento[] {
    return this.eventos.filter(evento =>
      evento.tema.toLocaleLowerCase().indexOf(this.filtroLista) !== -1 ||
      evento.local.toLocaleLowerCase().indexOf(this.filtroLista) !== -1);
  }

  private validation(): void {
    this.registerForm = this.fb.group({
      tema: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      local: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      dataEvento: ['', Validators.required],
      qtdPessoas: ['', [Validators.required, Validators.max(120000)]],
      imagemURL: ['', Validators.required],
      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public async salvarAlteracao(template: any) {
    if (!this.registerForm.valid) {
      this.toastr.warning('Formulário Inválido', 'ATENÇAO');
      return;
    }

    const response: any = await this.postUpload();
    if (response.error) {
      this.toastr.warning(response.error, 'ERROR Upload');
      return;
    }

    this.evento = Object.assign(this.evento || {}, this.registerForm.value);
    this.evento.imagemURL = this.fileName;
    this.fileName = '';

    if (this.evento.id) {
      this.update(template);
    } else {
      this.insert(template);
    }
  }

  private postUpload() {
    return new Promise((resolve, reject) => {
      this.eventosService.postUpload(this.file, this.fileName).pipe(takeUntil(this.unsubscribe))
        .subscribe(
          () => resolve({ error: null }),
          (e) => reject({ error: e })
        );
    });
  }

  private update(template: any): void {
    this.eventosService.putEvento(this.evento).pipe(takeUntil(this.unsubscribe)).subscribe(() => {
      this.getAllEventos();
      template.hide();
      this.toastr.success('Evento Atualizado', 'SUCESSO');
    }, error => console.log(error));
  }

  private insert(template: any): void {
    this.eventosService.postEvento(this.evento).pipe(takeUntil(this.unsubscribe))
      .subscribe(() => {
        this.getAllEventos();
        template.hide();
        this.toastr.success('Evento Inserido', 'SUCESSO');
      }, error => console.log(error));
  }

  public delete(id: number): void {
    this.eventosService.delete(id).pipe(takeUntil(this.unsubscribe)).subscribe(() => {
      this.getAllEventos();
      this.toastr.success('Evento Deletado', 'SUCESSO');
    }, error => console.log(error));
  }

  public onFileChange(event): void {
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files;
      this.fileName = this.file[0].name;
    }
  }

}
