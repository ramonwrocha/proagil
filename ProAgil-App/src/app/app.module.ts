import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TooltipModule, ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { EventosService } from './_services/eventos.service';

import { AppComponent } from './app.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { NavComponent } from './_layout/nav/nav.component';

import { DateTimeFormatPipe } from './_helpers/pipes/date-time-format.pipe';
import { PalestrantesComponent } from './components/palestrantes/palestrantes.component';
import { ContatosComponent } from './components/contatos/contatos.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TitleRouterComponent } from './_shared/title-router/title-router.component';

@NgModule({
   declarations: [
      AppComponent,
      DateTimeFormatPipe,
      EventosComponent,
      NavComponent,
      PalestrantesComponent,
      ContatosComponent,
      DashboardComponent,
      TitleRouterComponent
   ],
   imports: [
      AppRoutingModule,
      BrowserModule,
      BrowserAnimationsModule,
      BsDatepickerModule.forRoot(),
      BsDropdownModule.forRoot(),
      FormsModule,
      HttpClientModule,
      ModalModule.forRoot(),
      ReactiveFormsModule,
      TooltipModule.forRoot(),
      ToastrModule.forRoot()
   ],
   providers: [
     EventosService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
