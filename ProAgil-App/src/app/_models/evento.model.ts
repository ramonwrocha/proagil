import { Lote } from './lote.model';
import { RedeSocial } from './rede-social.model';
import { Palestrante } from './palestrante.model';

export interface Evento {
  id: number;
  local: string;
  dataEvento: Date;
  tema: string;
  qtdPessoas: number;
  imagemURL: string;
  telefone: string;
  email: string;
  lotes: Lote[];
  redesSociais: RedeSocial[];
  palestrantesEventos: Palestrante[];
}
