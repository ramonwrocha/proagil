import { Evento } from './evento.model';

export class Lote {
  id: number;
  nome: string;
  preco: number;
  dataInicio?: Date;
  dataFim?: Date;
  quantidade: number;
  eventoId: number;
}

// public int Id { get; set; }
//         public string Nome { get; set; }
//         public decimal Preco { get; set; }
//         public DateTime? DataInicio { get; set; }
//         public DateTime? DataFim { get; set; }
//         public int Quantidade { get; set; }
//         public int EventoId { get; set; }
//         public Evento Evento { get; }
