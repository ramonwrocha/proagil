import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Evento } from '../_models/evento.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from '../_helpers/utils/constants';

@Injectable({
  providedIn: 'root'
})
export class EventosService {

  baseUrl = `${Constants.BASE_URL}/api/evento`;

  constructor(private http: HttpClient) { }

  getAllEventos(): Observable<Evento[]> {
    return this.http.get<Evento[]>(this.baseUrl).pipe(map(data => data));
  }

  getEventosByTem(tema: string): Observable<Evento[]> {
    return this.http.get<Evento[]>(`${this.baseUrl}/tema/${tema}`).pipe(map(data => data));
  }

  getEventoById(id: number): Observable<Evento> {
    return this.http.get<Evento>(`${this.baseUrl}/${id}`).pipe(map(data => data));
  }

  putEvento(model: Evento): Observable<any> {
    return this.http.put(this.baseUrl, model).pipe(map(data => data));
  }

  postEvento(model: Evento): Observable<any> {
    return this.http.post(this.baseUrl, model).pipe(map(data => data));
  }

  delete(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  postUpload(file: File, fileName: string): Observable<any> {
    const fileToUpload = file[0] as File;
    const formData = new FormData();
    formData.append('file', fileToUpload, fileName);

    return this.http.post(`${this.baseUrl}/upload`, formData).pipe(map(data => data));
  }

}
