using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProAgil.Domain
{
    public class Evento
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage="Local deve ser preenchido")]
        public string Local { get; set; }
        public DateTime DataEvento { get; set; }
        [MaxLength(100, ErrorMessage="Tema deve ter máximo 100")]
        [MinLength(3, ErrorMessage="Tema deve ter minimo 3")]
        public string Tema { get; set; }
        public int QtdPessoas { get; set; }
        public string ImagemURL { get; set; } 
        public string Telefone { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public List<Lote> Lotes { get; set; }
        public List<RedeSocial> RedesSociais { get; set; }
        public List<PalestranteEvento> PalestrantesEventos { get; set; }
    }
}